package tp1.object;

/**
 * Nama : Rayza Arasj M.
 * NPM  : 1606876052
 */
public class Pemain {

    private String namaPemain;
    private String namaTim;
    private int nomorPemain;
    private int jumlahGol;
    private int jumlahPelanggaran;
    private int jumlahKartuKuning;
    private int jumlahKartuMerah;

    private static String[] arrNamaPemain =
            {"Arnold", "Kaidou", "Chopper", "Pica", "Enel", "Zoro", "Pedro", "Beckman", "Ace",
            "Shiryu", "Sakazuki", "Marco", "Garp", "Dadan", "Sengoku", "Sanji", "Magellan",
            "Dragon", "Sabo", "Smoker", "Luffy", "Franky", "Borsalino", "Buggy", "Crocodile",
            "Shanks", "Yasopp", "Coby", "Burgess", "Usopp", "Law", "Kid", "Bege", "Yonji",
            "Doffy", "Edward", "Mihawk", "Shanks", "Jinbei", "Killer", "Robin", "Roger",
            "Shiki", "Rayleigh", "Robb", "Kuma", "Moriah", "Teach", "Pagaya", "Conis", "Hachi",
            "Brook", "Kinemon", "Vergo", "Caesar", "Momo", "Mohji", "Cabaji", "Jozu", "Vista",
            "Doma", "Augur", "Drake", "Ivankov", "Charlotte", "Bellamy", "Demaro", "Dorry",
            "Brogy", "Kuro", "Zeff", "Gin", "Pearl", "Alvide", "Apoo", "Kuzan", "Nami", "Brook",
            "Hancock", "Koala"};

	/**
	 * Constructor for Pemain class
	 * @param namaPemain : name of the Pemain
	 * @param nomorPemain : number of the Pemain
	 */
	public Pemain(String namaPemain, int nomorPemain, String namaTim) {
        this.namaPemain 	   = namaPemain;
        this.namaTim		   = namaTim;
        this.nomorPemain	   = nomorPemain;
        this.jumlahGol 		   = 0;
        this.jumlahKartuKuning = 0;
        this.jumlahKartuMerah  = 0;
        this.jumlahPelanggaran = 0;
    }

    @Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append("Nomor        : " + this.nomorPemain + "\n");
		sb.append("Nama         : " + this.namaPemain + "\n");
		sb.append("Gol          : " + this.jumlahGol + "\n");
		sb.append("Pelanggaran  : " + this.jumlahPelanggaran + "\n");
		sb.append("Kartu Kuning : " + this.jumlahKartuKuning + "\n");
		sb.append("Kartu Merah  : " + this.jumlahKartuMerah);

		return sb.toString();
	}

	/*
	Adder methods
	 */
	public void addGol(int n) {
	    this.jumlahGol += n;
    }

    public void addKartuKuning(int n) {
	    this.jumlahKartuKuning += n;
    }

    public void addKartuMerah(int n) {
	    this.jumlahKartuMerah += n;
    }

    public void addPelanggaran(int n) {
	    this.jumlahPelanggaran += n;
    }

    /*
    Getters and Setters
     */
    public String getNamaPemain() {
        return namaPemain;
    }

    public void setNamaPemain(String namaPemain) {
        this.namaPemain = namaPemain;
    }

	public String getNamaTim() {
		return namaTim;
	}

	public void setNamaTim(String namaTim) {
		this.namaTim = namaTim;
	}

	public int getNomorPemain() {
        return nomorPemain;
    }

    public void setNomorPemain(int nomorPemain) {
        this.nomorPemain = nomorPemain;
    }

    public int getJumlahGol() {
        return jumlahGol;
    }

    public void setJumlahGol(int jumlahGol) {
        this.jumlahGol = jumlahGol;
    }

    public int getJumlahPelanggaran() {
        return jumlahPelanggaran;
    }

    public void setJumlahPelanggaran(int jumlahPelanggaran) {
        this.jumlahPelanggaran = jumlahPelanggaran;
    }

    public int getJumlahKartuKuning() {
        return jumlahKartuKuning;
    }

    public void setJumlahKartuKuning(int jumlahKartuKuning) {
        this.jumlahKartuKuning = jumlahKartuKuning;
    }

    public int getJumlahKartuMerah() {
        return jumlahKartuMerah;
    }

    public void setJumlahKartuMerah(int jumlahKartuMerah) {
        this.jumlahKartuMerah = jumlahKartuMerah;
    }

    public static String[] getArrNamaPemain() {
        return arrNamaPemain;
    }

    public static void setArrNamaPemain(String[] arrNamaPemain) {
        Pemain.arrNamaPemain = arrNamaPemain;
    }
}