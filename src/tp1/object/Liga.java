package tp1.object;

import tp1.utility.FormatHelper;
import tp1.utility.Randomizer;

import java.util.*;

/**
 * Nama : Rayza Arasj M.
 * NPM  : 1606876052
 */
public class Liga {

    private List<Tim> listTim;
    private List<Pemain> listPemain = new ArrayList<>();
    private List<Pemain> topSkorer;
    private int jumlahTim;

	/**
	 * Contructor for Liga class
	 * @param jumlahTim : number of teams that participate in the league
	 */
	public Liga (int jumlahTim) {
        this.jumlahTim = jumlahTim;
        listTim = new ArrayList<>(this.jumlahTim);

        int[] arrayAngkaUnikNama = Randomizer.prosesArrayAngkaUnik(this.jumlahTim, 0, 17);
        for (int i = 0; i < this.jumlahTim; i++) {
            String nama = Tim.getArrNamaTim()[arrayAngkaUnikNama[i]];
            Tim tim = new Tim(nama);
            this.listTim.add(tim);
            for (Pemain pemain : tim.getListPemain()) {
            	this.listPemain.add(pemain);
			}
        }
        this.updatePencetakGol();
		this.updatePeringkat();
    }

	/**
	 * get top scorer info
	 * @return : top skorer
	 */
	public String showPencetakGol() {
		StringBuilder sb = new StringBuilder();

		sb.append(" Peringkat | Nama Pemain | Nama Tim | Jumlah Gol \n");
		sb.append("-------------------------------------------------\n");

		for (int i = 0; i < this.topSkorer.size(); i++) {
			Pemain pemain = this.topSkorer.get(i);
			String peringkat = (i + 1) + "";
			String namaPemain = pemain.getNamaPemain();
			String namaTim = pemain.getNamaTim();
			String jumlahGol = pemain.getJumlahGol() + "";
			String info = String.format("%s|%s|%s|%s%n", FormatHelper.center(peringkat, 11),
					FormatHelper.center(namaPemain, 13), FormatHelper.center(namaTim, 10),
					FormatHelper.center(jumlahGol, 12));
			sb.append(info);
		}
		return sb.toString();
	}

	/**
	 * get standings info
	 * @return : standings
	 */
	public String showKlasemen() {
		StringBuilder sb = new StringBuilder();

		sb.append(" Peringkat | Nama Tim | Jumlah Gol | Jumlah Kebobolan | Menang | Kalah | Seri | Poin \n");
		sb.append("-------------------------------------------------------------------------------------\n");

		for (int i = 0; i < this.listTim.size(); i++) {
			Tim tim = this.listTim.get(i);
			String peringkat = (i + 1) + "";
			String namaTIm = tim.getNamaTim();
			String jumlahGol = tim.getJumlahGol() + "";
			String jumlahKebobolan = tim.getJumlahKebobolan() + "";
			String menang = tim.getJumlahMenang() + "";
			String kalah = tim.getJumlahKalah() + "";
			String seri = tim.getJumlahSeri() + "";
			String poin = tim.getJumlahPoin() + "";
			String info = String.format("%s|%s|%s|%s|%s|%s|%s|%s%n", FormatHelper.center(peringkat, 11),
					FormatHelper.center(namaTIm, 10), FormatHelper.center(jumlahGol, 12),
					FormatHelper.center(jumlahKebobolan, 18), FormatHelper.center(menang, 8),
					FormatHelper.center(kalah, 7), FormatHelper.center(seri, 6),
					FormatHelper.center(poin, 6));
			sb.append(info);
		}
		return sb.toString();
	}

	/**
	 * Process 10 highest top scorer
	 * @return list of pemain, size : 10
	 */
    public List<Pemain> prosesTopSkorer() {
		List<Pemain> listTemp = new ArrayList<>(10);

		listTemp.add(this.listPemain.get(0));

		for (int i = 1; i < this.listPemain.size(); i++) {
			Pemain pemainSekarang = this.listPemain.get(i);
			boolean flag = false;
			for (int j = 0; j < listTemp.size(); j++) {
				if (pemainSekarang.getJumlahGol() >= listTemp.get(j).getJumlahGol()) {
					listTemp.add(j, pemainSekarang);
					flag = true;
					break;
				}
			}
			if (!flag) {
				listTemp.add(pemainSekarang);
			}
		}
		return listTemp.subList(0, 10);
	}

    /**
     * Update the top scorer list
     */
	public void updatePencetakGol() {
    	this.topSkorer = this.prosesTopSkorer();
	}

	/**
	 * update this league standings
	 */
	public void updatePeringkat() {
		List<Tim> listTemp = new ArrayList<>(10);

		listTemp.add(this.listTim.get(0));

		for (int i = 1; i < this.listTim.size(); i++) {
			Tim timSekarang = this.listTim.get(i);
			boolean flag = false;
			for (int j = 0; j < listTemp.size(); j++) {
				int poinTimSekarang = timSekarang.getJumlahPoin();
				int poinTimTemp = listTemp.get(j).getJumlahPoin();
				if (poinTimSekarang > poinTimTemp) {
					listTemp.add(j, timSekarang);
					flag = true;
					break;
				} else if (poinTimSekarang == poinTimTemp) {
					int selisihGolTim = timSekarang.getJumlahGol() - timSekarang.getJumlahKebobolan();
					int selisihGolTemp = listTemp.get(j).getJumlahGol() - listTemp.get(j).getJumlahKebobolan();
					if (selisihGolTim > selisihGolTemp) {
						listTemp.add(j, timSekarang);
						flag = true;
						break;
					}
				}
			}
			if (!flag) {
				listTemp.add(timSekarang);
			}
		}
		this.listTim = listTemp;
	}

	/**
	 * Serch a tim on this liga's list of tim based on its name
	 * @param namaTim : tim's name that will be searched
	 * @return tim that have the name searched
	 */
	public Tim getTimDenganNama(String namaTim) {
		for (Tim tim : this.listTim) {
			if (tim.getNamaTim().equalsIgnoreCase(namaTim)) {
				return tim;
			}
		}
		System.out.println("Tidak ada tim dengan nama " + namaTim);
		return null;
	}

	/**
	 * Search a pemain on this liga's list of pemain based on its name
	 * @param namaTim : tim's name that will be searched
	 * @param namaPemain : pemain's name that will be searched
	 * @return pemain that have the name searched
	 */
	public Pemain getPemainDenganNama(String namaTim, String namaPemain) {
		Tim tim = this.getTimDenganNama(namaTim);
		if (tim != null) {
			for (Pemain pemain : tim.getListPemain()) {
				if (pemain.getNamaPemain().equalsIgnoreCase(namaPemain)) {
					return pemain;
				}
			}
			System.out.println("Tidak ada pemain di tim " + namaTim + " dengan nama " + namaPemain);
			return null;
		}
		return null;
	}

	/**
	 * Search a pemain on this liga's list of pemain based on its number
	 * @param namaTim : tim's name that will be searched
	 * @param nomorPemain : pemain's number that will be searched
	 * @return pemain that have the number searched
	 */
	public Pemain getPemainDenganNomor(String namaTim, int nomorPemain) {
		Tim tim = this.getTimDenganNama(namaTim);
		if (tim != null) {
			for (Pemain pemain : tim.getListPemain()) {
				if (pemain.getNomorPemain() == nomorPemain) {
					return pemain;
				}
			}
			System.out.println("Tidak ada pemain di tim " + namaTim + " dengan nomor " + nomorPemain);
			return null;
		}
		return null;
	}

    /*
    Getters and Setters
     */
	public List<Tim> getListTim() {
		return listTim;
	}

	public void setListTim(List<Tim> listTim) {
		this.listTim = listTim;
	}

	public int getJumlahTim() {
		return jumlahTim;
	}

	public void setJumlahTim(int jumlahTim) {
		this.jumlahTim = jumlahTim;
	}
}