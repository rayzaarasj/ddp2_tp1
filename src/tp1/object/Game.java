package tp1.object;

import tp1.utility.Randomizer;

import java.util.*;
import java.util.stream.IntStream;

/**
 * Nama : Rayza Arasj M.
 * NPM  : 1606876052
 */
public class Game {

	private boolean finished;
	private Liga liga;
    private List<Tim[]> matches;

	/**
	 * Contructor for Game class
	 * @param jumlahTim : number of team that will be initiated by Liga
	 */
	public Game (int jumlahTim) {
		liga = new Liga(jumlahTim);

		int jumlahPertandingan = (jumlahTim*(jumlahTim-1)) / 2;

		this.matches = new ArrayList<>(jumlahPertandingan);
		for (int i = 0; i < jumlahPertandingan; i++) {
			matches.add(new Tim[2]);
		}

		int counter = 0;
		for (int i = 0; i < jumlahTim; i++) {
            for (int j = i + 1; j < jumlahTim; j++) {
                this.matches.get(counter)[0] = liga.getListTim().get(i);
                this.matches.get(counter)[1] = liga.getListTim().get(j);
                counter++;
            }
        }

        Collections.shuffle(this.matches);
		this.finished = (this.matches.size() == 0) ? true : false;
	}

	/**
	 * Get next game info
	 * @return next match
	 */
	public String showNextGame() {
		Tim[] tim = this.matches.get(0);
		return tim[0].getNamaTim() + " VS " + tim[1].getNamaTim();
	}

    /**
     * Get the ending messages
     * @return : string of ending messages
     */
	public String showEndCard() {
		StringBuilder sb = new StringBuilder();
		sb.append("LIGA FASILKOM MUSIM INI TELAH USAI\n\n");
		sb.append(String.format("CHAMPION: %s%n%n%n", this.liga.getListTim().get(0).getNamaTim()));
		sb.append("KLASEMEN\n");
		sb.append(this.liga.showKlasemen());
		sb.append("\n\nTOP SCORE\n");
		sb.append(this.liga.showPencetakGol());
		sb.append("\n\nGOODBYE...");
		return sb.toString();
	}

    /**
     * Play nextgame manually from user input
     * @param arrInput user input
     * @return match statistic / match info
     */
	public String nextGameManual(String[] arrInput) {
	    Tim[] timMain = this.matches.get(0);

	    Tim timA = timMain[0];
	    Tim timB = timMain[1];

        boolean commandBenar = this.checkCommand(arrInput);
        if (!commandBenar) {
            return "Pastikan command yang dimasukan benar";
        }
	    boolean timBenar = this.checkTim(timA, timB, arrInput);
	    if (!timBenar) {
	        return "Pastikan tim yang dimasukan bermain di pertandingan selanjutnya!";
        }
	    boolean pemainBenar = this.checkPemain(arrInput);
	    if (!pemainBenar) {
	        return null;
        }

	    int[][] allData = this.prosesDataManual(timA, timB, arrInput);

        int golA = IntStream.of(allData[0]).sum();
        int kartuKuningA = IntStream.of(allData[1]).sum();
        int kartuMerahA = IntStream.of(allData[2]).sum();
        int pelanggaranA = IntStream.of(allData[3]).sum();
        int[] dataGA = allData[0];
        int[] dataKKA = allData[1];
        int[] dataKMA = allData[2];
        int[] dataPA = allData[3];
        this.prosesGTim(timA, dataGA);
        this.prosesKKTim(timA, dataKKA);
        this.prosesKMTim(timA, dataKMA);
        this.prosesPTim(timA, dataPA);


        int golB = IntStream.of(allData[4]).sum();
        int kartuKuningB = IntStream.of(allData[5]).sum();
        int kartuMerahB = IntStream.of(allData[6]).sum();
        int pelanggaranB = IntStream.of(allData[7]).sum();
        int[] dataGB = allData[4];
        int[] dataKKB = allData[5];
        int[] dataKMB = allData[6];
        int[] dataPB = allData[7];
        this.prosesGTim(timB, dataGB);
        this.prosesKKTim(timB, dataKKB);
        this.prosesKMTim(timB, dataKMB);
        this.prosesPTim(timB, dataPB);

        this.prosesTim(timA, timB, golA, golB);
        liga.updatePeringkat();
        liga.updatePencetakGol();
        this.finished = (this.matches.size() == 0) ? true : false;

        StringBuilder sb = new StringBuilder();

        sb.append(String.format("%nStatistika Pertandingan Tim %s VS Tim %s%n%n", timA.getNamaTim(), timB.getNamaTim()));

        sb.append(String.format("Tim : %s%n", timA.getNamaTim()));
        sb.append(String.format("Gol : %d%n", golA));
        sb.append(String.format("Pelanggaran : %d%n", (pelanggaranA+kartuKuningA+kartuMerahA)));
        sb.append(String.format("Kartu Kuning : %d%n", kartuKuningA));
        sb.append(String.format("Kartu Merah : %d%n%n", kartuMerahA));

        sb.append(String.format("Tim : %s%n", timB.getNamaTim()));
        sb.append(String.format("Gol : %d%n", golB));
        sb.append(String.format("Pelanggaran : %d%n", (pelanggaranB+kartuKuningB+kartuMerahB)));
        sb.append(String.format("Kartu Kuning : %d%n", kartuKuningB));
        sb.append(String.format("Kartu Merah : %d%n", kartuMerahB));

        this.matches.remove(0);
        return sb.toString();
    }

    /**
     * Process data of nextgame based on input
     * @param timA : Team A (playing)
     * @param timB : Team B (playing)
     * @param arrInput input from user
     * @return array of data, where it holds number of goal, yellow card, red card, and foul for
     *         each team
     */
    public int[][] prosesDataManual(Tim timA, Tim timB, String[] arrInput) {
	    int[][] arrTemp = new int[8][5];
	    String namaTimA = timA.getNamaTim();
	    for (int i = 1; i < arrInput.length; i += 3) {
	        switch (arrInput[i].toUpperCase()) {
                case "-G" :
                    if (arrInput[i+1].equals(namaTimA)) {
                        arrTemp[0][timA.getIndexPemain(Integer.parseInt(arrInput[i+2]))] += 1;
                    } else {
                        arrTemp[4][timB.getIndexPemain(Integer.parseInt(arrInput[i+2]))] += 1;
                    }
                    break;
                case "-KK" :
                    if (arrInput[i+1].equals(namaTimA)) {
                        arrTemp[1][timA.getIndexPemain(Integer.parseInt(arrInput[i+2]))] += 1;
                    } else {
                        arrTemp[5][timB.getIndexPemain(Integer.parseInt(arrInput[i+2]))] += 1;
                    }
                    break;
                case "-KM" :
                    if (arrInput[i+1].equals(namaTimA)) {
                        arrTemp[2][timA.getIndexPemain(Integer.parseInt(arrInput[i+2]))] += 1;
                    } else {
                        arrTemp[6][timB.getIndexPemain(Integer.parseInt(arrInput[i+2]))] += 1;
                    }
                    break;
                case "-P" :
                    if (arrInput[i+1].equals(namaTimA)) {
                        arrTemp[3][timA.getIndexPemain(Integer.parseInt(arrInput[i+2]))] += 1;
                    } else {
                        arrTemp[7][timB.getIndexPemain(Integer.parseInt(arrInput[i+2]))] += 1;
                    }
                    break;
                default:
            }
        }

        return arrTemp;
    }

    /**
     * check if user input is correct for command section
     * @param arrInput : user input
     * @return true if user input is correct
     */
    public boolean checkCommand(String[] arrInput) {
        for (int i = 1; i < arrInput.length; i += 3) {
            String command = arrInput[i].toUpperCase();
            if (!command.equals("-G") && !command.equals("-KK") && !command.equals("-KM") && !command.equals("-P")) {
                return false;
            }
        }
        return true;
    }

    /**
     * check if user input is correct for team section
     * @param timA : Team A (playing)
     * @param timB : Team B (playing)
     * @param arrInput : user input
     * @return true if user input is correct
     */
    public boolean checkTim(Tim timA, Tim timB, String[] arrInput) {
        for (int i = 2; i < arrInput.length; i += 3) {
            String namaTim = arrInput[i];
            if(!(namaTim.equals(timA.getNamaTim()) ^ namaTim.equals(timB.getNamaTim()))) {
                return false;
            }
        }
        return true;
    }

    /**
     * check if user input is correct for player section
     * @param arrInput : user input
     * @return true if user input is correct
     */
    public boolean checkPemain(String[] arrInput) {
        System.out.println(arrInput.length);
        for (int i = 2; i < arrInput.length; i += 3) {
	        int nomorPemain = Integer.parseInt(arrInput[i + 1]);
	        String namaTim = arrInput[i];
	        Pemain pemain = this.getLiga().getPemainDenganNomor(namaTim, nomorPemain);
	        if (pemain == null) {
	            return false;
            }
        }
        if (arrInput.length <= 2) {
            throw new IndexOutOfBoundsException();
        }
        return true;
    }

    /**
     * Play all remaining game of tournamanet
     * @return : String of all the information from each game
     */
	public String nextGameAll() {
		StringBuilder sb = new StringBuilder();
		int size = this.matches.size();
		for(int i = 0; i < size; i++) {
			sb.append(this.nextGame());
		}
		return sb.toString();
	}

    /**
     * Play nextgamme randomly
     * @return : information of played game
     */
	public String nextGame() {
	    Random random = new Random();
	    Tim[] timMain = this.matches.remove(0);


	    Tim timA = timMain[0];
	    int golA = random.nextInt(6);
		int kartuKuningA = random.nextInt(4);
	    int kartuMerahA = random.nextInt(3);
	    int pelanggaranA = random.nextInt(6);
		int[] dataGA = Randomizer.prosesStatistikPemain(golA, timA.getUKURAN_TIM());
	    int[] dataKKA = Randomizer.prosesStatistikPemain(kartuKuningA, timA.getUKURAN_TIM());
	    int[] dataKMA = Randomizer.prosesStatistikPemain(kartuMerahA, timA.getUKURAN_TIM());
	    int[] dataPA = Randomizer.prosesStatistikPemain(pelanggaranA, timA.getUKURAN_TIM());
	    this.prosesGTim(timA, dataGA);
	    this.prosesKKTim(timA, dataKKA);
	    this.prosesKMTim(timA, dataKMA);
	    this.prosesPTim(timA, dataPA);


	    Tim timB = timMain[1];
		int golB = random.nextInt(6);
		int kartuKuningB = random.nextInt(4);
		int kartuMerahB = random.nextInt(3);
		int pelanggaranB = random.nextInt(6);
		int[] dataGB = Randomizer.prosesStatistikPemain(golB, timB.getUKURAN_TIM());
		int[] dataKKB = Randomizer.prosesStatistikPemain(kartuKuningB, timB.getUKURAN_TIM());
		int[] dataKMB = Randomizer.prosesStatistikPemain(kartuMerahB, timB.getUKURAN_TIM());
		int[] dataPB = Randomizer.prosesStatistikPemain(pelanggaranB, timB.getUKURAN_TIM());
		this.prosesGTim(timB, dataGB);
		this.prosesKKTim(timB, dataKKB);
		this.prosesKMTim(timB, dataKMB);
		this.prosesPTim(timB, dataPB);

		this.prosesTim(timA, timB, golA, golB);
		liga.updatePeringkat();
		liga.updatePencetakGol();
		this.finished = (this.matches.size() == 0) ? true : false;

		StringBuilder sb = new StringBuilder();

		sb.append(String.format("%nStatistika Pertandingan Tim %s VS Tim %s%n%n", timA.getNamaTim(), timB.getNamaTim()));

		sb.append(String.format("Tim : %s%n", timA.getNamaTim()));
		sb.append(String.format("Gol : %d%n", golA));
		sb.append(String.format("Pelanggaran : %d%n", (pelanggaranA+kartuKuningA+kartuMerahA)));
		sb.append(String.format("Kartu Kuning : %d%n", kartuKuningA));
		sb.append(String.format("Kartu Merah : %d%n%n", kartuMerahA));

		sb.append(String.format("Tim : %s%n", timB.getNamaTim()));
		sb.append(String.format("Gol : %d%n", golB));
		sb.append(String.format("Pelanggaran : %d%n", (pelanggaranB+kartuKuningB+kartuMerahB)));
		sb.append(String.format("Kartu Kuning : %d%n", kartuKuningB));
		sb.append(String.format("Kartu Merah : %d%n", kartuMerahB));

		return sb.toString();
	}

    /**
     * Process team information based on goals
     * @param timA : Team A
     * @param timB : Team B
     * @param golA : Goal of Team A
     * @param golB : Goal of Team B
     */
	public void prosesTim(Tim timA, Tim timB, int golA, int golB) {
		if (golA > golB) {
			timA.addMenang(1);
			timA.addGol(golA);
			timA.addKebobolan(golB);
			timB.addKalah(1);
			timB.addGol(golB);
			timB.addKebobolan(golA);
		} else if (golA < golB) {
			timA.addKalah(1);
			timA.addGol(golA);
			timA.addKebobolan(golB);
			timB.addMenang(1);
			timB.addGol(golB);
			timB.addKebobolan(golA);
		} else {
			timA.addSeri(1);
			timA.addGol(golA);
			timA.addKebobolan(golB);
			timB.addSeri(1);
			timB.addGol(golB);
			timB.addKebobolan(golA);
		}
		timA.updatePoin();
		timB.updatePoin();
	}

    /**
     * Process goal for tim's pemain
     * @param tim : tim that will be processed
     * @param data : data that will be processed
     */
	public void prosesGTim(Tim tim, int[] data) {
		List<Pemain> listPemain = tim.getListPemain();
		for (int i = 0; i < data.length; i++) {
			listPemain.get(i).addGol(data[i]);
		}
	}

    /**
     * Process yellow card for tim's pemain
     * @param tim : tim that will be processed
     * @param data : data that will be processed
     */
	public void prosesKKTim(Tim tim, int[] data) {
		List<Pemain> listPemain = tim.getListPemain();
		for (int i = 0; i < data.length; i++) {
			listPemain.get(i).addKartuKuning(data[i]);
			listPemain.get(i).addPelanggaran(data[i]);
			if (data[i] >= 2) {
				listPemain.get(i).addKartuMerah(1);
			}
		}
	}

    /**
     * Process red card for tim's pemain
     * @param tim : tim that will be processed
     * @param data : data that will be processed
     */
	public void prosesKMTim(Tim tim, int[] data) {
		List<Pemain> listPemain = tim.getListPemain();
		for (int i = 0; i < data.length; i++) {
			listPemain.get(i).addKartuMerah(data[i]);
			listPemain.get(i).addPelanggaran(data[i]);
		}
	}

    /**
     * Process foul for tim's pemain
     * @param tim : tim that will be processed
     * @param data : data that will be processed
     */
	public void prosesPTim(Tim tim, int[] data) {
		List<Pemain> listPemain = tim.getListPemain();
		for (int i = 0; i < data.length; i++) {
			listPemain.get(i).addPelanggaran(data[i]);
		}
	}

	/*
	Getters and Setters
	 */
	public boolean isFinished() {
		return finished;
	}

	public void setFinished(boolean finished) {
		this.finished = finished;
	}

	public Liga getLiga() {
		return liga;
	}

	public void setLiga(Liga liga) {
		this.liga = liga;
	}

	public List<Tim[]> getMatches() {
		return matches;
	}

	public void setMatches(List<Tim[]> matches) {
		this.matches = matches;
	}
}