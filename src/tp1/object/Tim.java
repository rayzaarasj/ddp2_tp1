package tp1.object;

import tp1.utility.FormatHelper;
import tp1.utility.Randomizer;

import java.util.*;

/**
 * Nama : Rayza Arasj M.
 * NPM  : 1606876052
 */
public class Tim {

	private final int UKURAN_TIM = 5;

	private List<Pemain> listPemain = new ArrayList<>(UKURAN_TIM);
    private String namaTim;
    private int peringkat;
    private int jumlahMenang;
    private int jumlahKalah;
    private int jumlahSeri;
    private int jumlahPoin;
    private int jumlahGol;
    private int jumlahKebobolan;

    private static String[] arrNamaTim =
			{"Gajah", "Rusa", "Belalang", "Kodok", "Kucing", "Tupai", "Rajawali", "Siput", "Kumbang",
			"Semut", "Ular", "Harimau", "Kuda", "Serigala", "Panda", "Kadal", "Ayam", "Bebek"};

	/**
	 * Constructor for Tim class
	 * @param namaTim : name of the Tim
	 */
	public Tim(String namaTim) {
        this.namaTim 		 = namaTim;
        this.peringkat 		 = 1;
        this.jumlahMenang 	 = 0;
        this.jumlahKalah 	 = 0;
        this.jumlahSeri 	 = 0;
        this.jumlahPoin		 = this.hitungPoin();
        this.jumlahGol		 = 0;
        this.jumlahKebobolan = 0;

        int[] arrayAngkaUnikNomor = Randomizer.prosesArrayAngkaUnik(UKURAN_TIM, 1, 99);
        List<String> namaUnik = Randomizer.prosesNamaUnik(UKURAN_TIM, Pemain.getArrNamaPemain());

		for (int i = 0; i < UKURAN_TIM; i++) {
			String nama = namaUnik.get(i);
			int nomor = arrayAngkaUnikNomor[i];
			this.listPemain.add(new Pemain(nama, nomor, namaTim));
		}
	}

	/**
	 * Calculate this Tim's point
	 * @return point of Tim based on it's match history
	 */
	private int hitungPoin() {
    	return ((this.jumlahMenang * 3) + (this.jumlahSeri * 1) + (this.jumlahKalah * 0));
	}

    /**
     * Update Tim's point
     */
	public void updatePoin() {
	    this.jumlahPoin = this.hitungPoin();
    }

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();

		sb.append(" Nomor | Nama Pemain | Gol | Pelanggaran | Kartu Kuning | Kartu Merah \n");
		sb.append("----------------------------------------------------------------------\n");

		for (Pemain pemain : this.listPemain) {
			String nomorPemain = pemain.getNomorPemain() + "";
			String namaPemain = pemain.getNamaPemain();
			String jumlahGol = pemain.getJumlahGol() + "";
			String jumlahPelanggaran = pemain.getJumlahPelanggaran() + "";
			String jumlahKartuKuning = pemain.getJumlahKartuKuning() + "";
			String jumlahKartuMerah = pemain.getJumlahKartuMerah() + "";
			String info = String.format("%s|%s|%s|%s|%s|%s%n", FormatHelper.center(nomorPemain, 7),
					FormatHelper.center(namaPemain, 13), FormatHelper.center(jumlahGol, 5),
					FormatHelper.center(jumlahPelanggaran, 13), FormatHelper.center(jumlahKartuKuning, 14),
					FormatHelper.center(jumlahKartuMerah, 13));
			sb.append(info);
		}
		return sb.toString();
	}

    /**
     * get index player from its number
     * @param nomorPemain : players's number
     * @return player index, -1 if not found
     */
	public int getIndexPemain(int nomorPemain) {
	    for (int i = 0; i < this.listPemain.size(); i++) {
	        if (this.listPemain.get(i).getNomorPemain() == nomorPemain) {
	            return i;
            }
        }
        return -1;
    }

	/*
	Adder method
	 */

	public void addMenang(int n) {
		this.jumlahMenang += n;
	}

	public void addKalah(int n) {
		this.jumlahKalah += n;
	}

	public void addSeri(int n) {
		this.jumlahSeri += n;
	}

	public void addGol(int n) {
		this.jumlahGol += n;
	}

	public void addKebobolan(int n) {
		this.jumlahKebobolan += n;
	}

    /*
    Getters and Setters
     */

	public List<Pemain> getListPemain() {
		return listPemain;
	}

	public void setListPemain(List<Pemain> listPemain) {
		this.listPemain = listPemain;
	}

	public String getNamaTim() {
		return namaTim;
	}

	public void setNamaTim(String namaTim) {
		this.namaTim = namaTim;
	}

	public int getPeringkat() {
		return peringkat;
	}

	public void setPeringkat(int peringkat) {
		this.peringkat = peringkat;
	}

	public int getJumlahMenang() {
		return jumlahMenang;
	}

	public void setJumlahMenang(int jumlahMenang) {
		this.jumlahMenang = jumlahMenang;
	}

	public int getJumlahKalah() {
		return jumlahKalah;
	}

	public void setJumlahKalah(int jumlahKalah) {
		this.jumlahKalah = jumlahKalah;
	}

	public int getJumlahSeri() {
		return jumlahSeri;
	}

	public void setJumlahSeri(int jumlahSeri) {
		this.jumlahSeri = jumlahSeri;
	}

	public int getJumlahPoin() {
		return jumlahPoin;
	}

	public void setJumlahPoin(int jumlahPoin) {
		this.jumlahPoin = jumlahPoin;
	}

	public int getJumlahGol() {
		return jumlahGol;
	}

	public void setJumlahGol(int jumlahGol) {
		this.jumlahGol = jumlahGol;
	}

	public int getJumlahKebobolan() {
		return jumlahKebobolan;
	}

	public void setJumlahKebobolan(int jumlahKebobolan) {
		this.jumlahKebobolan = jumlahKebobolan;
	}

	public static String[] getArrNamaTim() {
		return arrNamaTim;
	}

	public static void setArrNamaTim(String[] arrNamaTim) {
		Tim.arrNamaTim = arrNamaTim;
	}

	public int getUKURAN_TIM() {
		return UKURAN_TIM;
	}
}