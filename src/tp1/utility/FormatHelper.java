package tp1.utility;

/**
 * Nama : Rayza Arasj M.
 * NPM  : 1606876052
 */
public class FormatHelper {

	/**
	 * make center align formatting
	 * @param kata : word that will be formatted
	 * @param ukuran : space of formatting
	 * @return formatted word
	 */
	public static String center(String kata, int ukuran) {
		StringBuilder sb = new StringBuilder();
		for (int i = 0; i < (ukuran - kata.length()) / 2; i++) {
		    sb.append(" ");
        }
        sb.append(kata);
        while (sb.length() < ukuran) {
            sb.append(" ");
        }
		return sb.toString();
	}
}