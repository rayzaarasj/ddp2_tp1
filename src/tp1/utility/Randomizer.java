package tp1.utility;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

/**
 * Nama : Rayza Arasj M.
 * NPM  : 1606876052
 */
public class Randomizer {
    
    /**
     * Process an array of unique random numbers
     * @param ukuran : size of returned array
     * @param min    : minimal value of generated int
     * @param max    : maximal value of generated int
     * @return : array of random integers
     */
    public static int[] prosesArrayAngkaUnik(int ukuran, int min, int max) {
        Random random = new Random();

        List<Integer> listTemp = new ArrayList<>();
        for (int i = min; i <= max; i++) {
            listTemp.add(i);
        }

        int[] arrTemp = new int[ukuran];
        for (int i = 0; i < arrTemp.length; i++) {
            arrTemp[i] = listTemp.remove(random.nextInt(listTemp.size()));
        }

        return arrTemp;
    }

    /**
     * Process a stat (ex. : goal) to be randomly distributed between a size of array
     * @param jumlahStatistik : amount of stat that will be distributed
     * @param ukuran : size of array that will be the recipient of distribution
     * @return : array of int with sum of jumlahStatistik
     */
    public static int[] prosesStatistikPemain(int jumlahStatistik, int ukuran) {
        Random random = new Random();
        int[] arrTemp = new int[ukuran];

        int jumlah = jumlahStatistik;

        while (jumlah > 0) {
            for (int i = 0; i < arrTemp.length; i++) {
                int randint = random.nextInt(jumlah + 1);
                arrTemp[i] += randint;
                jumlah -= randint;
                if (jumlah == 0) {
                    break;
                }
            }
        }
		return arrTemp;
    }

    /**
     * Proses a list of unique names
     * @param ukuran : size of list
     * @param arrNama : origin of names
     * @return : unique names
     */
    public static List<String> prosesNamaUnik(int ukuran, String[] arrNama) {
        List<String> listTemp = new ArrayList<>();
        while (listTemp.size() < ukuran) {
            Random random = new Random();
            int randint = random.nextInt(arrNama.length);
            String nama = arrNama[randint];

            if (!listTemp.contains(nama)) {
                listTemp.add(nama);
            }
        }
        return listTemp;
    }
}