package tp1;

import tp1.object.Game;
import tp1.object.Pemain;
import tp1.object.Tim;

import java.util.Scanner;

/**
 * Created by rayzaarasj on 13/03/2017.
 */
public class Main {

	public static void main(String[] args) {
		boolean initiated = false;
		Scanner scanner = new Scanner(System.in);

		Game game = null;

		while (!initiated) {
			System.out.print("WELCOME..\n[CSLEAGUE2017]=> ");
			String input = scanner.nextLine();
			String[] arrInput = input.split(" ");

			try {
				if (arrInput[0].toUpperCase().equals("INIT")) {
					int jumlahTim = Integer.parseInt(arrInput[1]);
					if (4 <= jumlahTim && jumlahTim <= 10) {
						game = new Game(jumlahTim);
                        System.out.println("Game berhasil diinisiasi!");
                        initiated = true;
					} else {
						System.out.println("Jumlah tim tidak sesuai batasan!");
					}
				} else {
					System.out.println("Game harus dimulai terlebih dahulu!");
				}
			} catch (IndexOutOfBoundsException e) {
				System.out.println("Tolong masukan jumlah tim yang sesuai!");
			} catch (NumberFormatException e) {
				System.out.println("Tolong masukan jumlah tim yang sesuai!");
			}
		}

		while (!game.isFinished()) {
			System.out.print("[CSLEAGUE2017]=> ");
			String input = scanner.nextLine();
			String[] arrInput = input.split(" ");
			prosesInput(arrInput, game);
		}

		String info = game.showEndCard();
		System.out.println(info);
	}

    /**
     * proses user input
     * @param arrInput user input
     * @param game : game that is being played
     */
	public static void prosesInput(String[] arrInput, Game game) {
		switch (arrInput[0].toUpperCase()) {
			case "NEXTGAME" :
				prosesNextGame(arrInput, game);
				break;
			case "SHOW" :
				prosesShow(arrInput, game);
				break;
			default :
				System.out.println("Command tidak ditemukan!");
		}
	}

    /**
     * proses user input, for nextgame types
     * @param arrInput user input
     * @param game game that is being played
     */
	public static void prosesNextGame(String[] arrInput, Game game) {
		try {
			if (arrInput.length == 1) {
                String info = game.nextGame();
				System.out.println(info);
				return;
			}
			switch (arrInput[1].toUpperCase()) {
                case "-ALL" :
                	String info = game.nextGameAll();
					System.out.println(info);
					break;
				case "-G" :
				    String info2 = game.nextGameManual(arrInput);
				    if (info2 != null) {
                        System.out.println(info2);
                    }
                    break;
				default:
					System.out.println("Command nextGame tidak ditemukan!");
			}
		} catch (IndexOutOfBoundsException e) {
            System.out.println("Tolong masukan perintah nextgame yang lengkap!");
        }
	}

    /**
     * proses user input, show types
     * @param arrInput user input
     * @param game game that is being played
     */
	public static void prosesShow(String[] arrInput, Game game) {
		try {
			switch (arrInput[1].toUpperCase()) {
				case "KLASEMEN" :
					String klasemen = game.getLiga().showKlasemen();
					System.out.println(klasemen);
					break;
				case "PENCETAKGOL" :
					String pencetakGol = game.getLiga().showPencetakGol();
					System.out.println(pencetakGol);
					break;
				case "TIM" :
					Tim tim = game.getLiga().getTimDenganNama(arrInput[2]);
					if (tim != null) {
						System.out.println(tim);
					}
					break;
				case "PEMAIN" :
					String namaTim = arrInput[2];
					try {
						int nomorPemain = Integer.parseInt(arrInput[3]);
						Pemain pemain = game.getLiga().getPemainDenganNomor(namaTim, nomorPemain);
						if (pemain != null) {
							System.out.println(pemain);
						}
					} catch (NumberFormatException e) {
						String namaPemain = arrInput[3];
						Pemain pemain = game.getLiga().getPemainDenganNama(namaTim, namaPemain);
						if (pemain != null) {
							System.out.println(pemain);
						}
					}
					break;
				case "NEXTGAME" :
					System.out.println(game.showNextGame());
					break;
				default :
					System.out.println("Command show tidak ditemukan!");
			}
		} catch (IndexOutOfBoundsException e) {
			System.out.println("Tolong masukan perintah show yang lengkap!");
		}
	}
}